# Benchmarking the Techniques for the Evolution of Variant-Rich Systems

Materials for the research paper:

* Daniel Strüber, Mukelabai Mukelabai, Jacob Krüger, Stefan Fischer, Lukas Linsbauer, Jabier Martinez, Thorsten Berger.
  Facing the Truth: Benchmarking the Techniques for the Evolution of Variant-Rich Systems.
  In 23rd International Systems and Software Product Line Conference - Volume A (SPLC’19), September 9–13, 2019, Paris, France. ACM, New York, NY, USA, 12 pages.
  https://doi.org/10.1145/3336294.3336302

## Replication package

Downoad the replication package: [benchmark-survey-reppackage.zip](https://bitbucket.org/easelab/evobench/src/master/benchmark-survey-reppackage.zip)

## Additional information for surveyed benchmarks

|  No | Name              | Original publication                                                                                       | URL                                                                |
|-----|-------------------|------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| 1   | ArgoUml           |  [SPLC'18a](https://doi.org/10.1145/3233027.3236402)                                                       | https://github.com/marcusvnac/argouml-spl                          |
| 2   | Drupal            |  [SoSyM'17](https://doi.org/10.1007/s10270-015-0459-z)                                                     | http://www.isa.us.es/anabsanchez-sosym14                           |
| 3   | EFLBench          |  [IST'18](https://doi.org/10.1016/j.infsof.2018.07.005)                                                    | https://github.com/but4reuse/but4reuse/wiki/Benchmarks             |
| 4   | Linux Kernel      |  [ICSE'13](https://doi.org/10.1109/ICSE.2013.6606705)                                                      | https://sites.google.com/site/yinxingxue/home/projects/flbenchmark |
| 5   | Marlin + BCWallet |  [JSS'19](https://www.sciencedirect.com/science/article/pii/S0164121219300184)                                            | https://github.com/hui8958/Marlin/tree/MarlinFeatureAnnotations    |
| 6   | Clafer WebTools   | [SPLC'15](https://gsd.uwaterloo.ca/sites/default/files/ji-2015-annotations.pdf)                            | https://github.com/redmagic4                                       |
| 7   | DoSC              |  [MSR'17](https://doi.org/10.1109/MSR.2017.49)                                                             | https://github.com/Chenguang-Zhu/DoSC                              |
| 8   | SystemsSwVarModel |  [TSE'13](https://ieeexplore.ieee.org/ielx7/32/6674042/06572787.pdf?tp=&arnumber=6572787&isnumber=6674042) | https://bitbucket.org/tberger/variability-models                   |                                      |
| 9   | TraceLab CoEST    |  [ICSE'12](http://dl.acm.org/citation.cfm?id=2337223.2337422)                                              | http://www.coest.org/                                              |
| 10  | Bug database      |  [TSE'18](https://dl.acm.org/citation.cfm?doid=3177743.3149119)   									       | http://vbdb.itu.dk/                                                |